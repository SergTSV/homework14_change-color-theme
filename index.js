const root = document.querySelector(':root');
const btn = document.querySelector('.btn')

const firstTheme = {
    nameTheme: 'firstTheme',
  logo: '#034f77',
  logoBorder: '#bdbbbb',
  body: '#8d8d8d',
  bodyBack: '#868585',
  navbar: '#4a5156',
  navbarHover: '#464545',
  leftMenu: '#485863',
  leftMenuHover: '#464545',
  articleText: '#b4b3b3',
  footerMenu: '#b3b6b6',
  footerMenuHover: '#81d3f3',
  copyLeft: '#010101',
  footerMenuBack: 'rgba(158, 169, 176, 0.48)'
};

const twoTheme = {
    nameTheme: 'twoTheme',
    logo: '#4BCAFF',
    logoBorder: '#000',
    body: '#fff',
    bodyBack: '#f5f5f5',
    navbar: '#35444F',
    navbarHover: '#222F3A',
    leftMenu: '#485863',
    leftMenuHover: '#DBDBDB',
    articleText: '#2C2C2C',
    footerMenu: '#0C1927',
    footerMenuHover: '#3181A6',
    copyLeft: '#010101',
    footerMenuBack: 'rgba(99, 105, 110, 0.48)'
};
let switchTheme = 'twoTheme';

let currentTheme = JSON.parse(localStorage.getItem('theme'));

if (currentTheme) {
    if (currentTheme.nameTheme === 'twoTheme') {
        switchTheme = 'firstTheme';
    }} else {
    currentTheme = twoTheme;
    }

changeTheme = function (){
    if (switchTheme === 'firstTheme'){
        switchTheme = 'twoTheme';
        currentTheme = twoTheme;
        for (let key in currentTheme){
            if (key !== 'nameTheme'){
                root.style.setProperty('--'+key, currentTheme[key]);
            }}
    }else{
        switchTheme = 'firstTheme';
        currentTheme = firstTheme;
        for (let key in currentTheme){
            if (key !== 'nameTheme'){
                root.style.setProperty('--'+key, currentTheme[key]);
            }}
    }
};

btn.addEventListener('click', () => {
    changeTheme();
    localStorage.setItem('theme', JSON.stringify(currentTheme));

});

changeTheme();
